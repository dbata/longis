import { QueryExpression, QueryEntity, QueryField } from '@themost/query';
import { UserContext } from '../UserContext';
import { SelectQueryUtils } from './SelectQueryUtils';

/**
 * @param {import('@themost/data').DataEventArgs} event 
 * @returns 
 */
async function beforeExecuteAsync(event) {
    // get context
    const context = event.model.context;
    // get user context
    const userContext = new UserContext(context);
    /**
     * get query
     * @type {QueryExpression}
     */
    const query = event.emitter && event.emitter.query;
    // validate query type
    const selectQuery = new SelectQueryUtils(query);
    if (selectQuery.isSelectQuery() === false) {
        return;
    }
    if (selectQuery.alreadyJoinedWith('SelectTimeTableEvents')) {
        return;
    }
    if (userContext.scopes.includes('registrar') === false) {
        return;
    }
    // get user groups
    const isRegistrarAssistant = await userContext.is('RegistrarAssistants');
    if (isRegistrarAssistant === false) {
        return;
    }
    // get user
    const user = await userContext.user;
    // join user study program courses
    const UserStudyPrograms = new QueryEntity('UserStudyPrograms').as('SelectStudyPrograms');
    const TimetableEvents = new QueryEntity(query.$collection);
    const secondQuery = new QueryExpression().from('TimeTableStudyPrograms')
    .select(
        new QueryField('valueId').as('timetableEvent')
    ).as('SelectTimeTableEvents').join(UserStudyPrograms).with(
        new QueryExpression()
        .where(
            new QueryField('parentId').from('TimeTableStudyPrograms')
        ).equal(
            new QueryField('studyProgram').from(UserStudyPrograms)
        ).and(
            new QueryField('user').from(UserStudyPrograms)
        ).equal(
            user.id
        )
    ).distinct();
    query.join(secondQuery).with(
        new QueryExpression()
        .where(
            new QueryField('id').from(TimetableEvents)
        ).equal(
            new QueryField('timetableEvent').from('SelectTimeTableEvents')
        )
    );
}


/**
 * @param {import('@themost/data').DataEventArgs} event 
 * @param {Function} callback 
 * @returns 
 */
function beforeExecute(event, callback) {
    return beforeExecuteAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

export {
    beforeExecute
}