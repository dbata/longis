/* eslint-disable no-unused-vars */
import { DataError, DataNotFoundError } from '@themost/common';
import { DataEventArgs } from '@themost/data';
import { ExpressDataContext } from '@themost/express';

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
  /**
   * @type {ExpressDataContext}
   */
  const context = event.model.context;
  const specializationCourse = event.target;
  const studyProgramID = specializationCourse.studyProgramCourse.studyProgram;
  const course = specializationCourse.studyProgramCourse.course;

  const studyProgram = await context.model('StudyProgram')
    .where('id').equal(studyProgramID)
    .silent()
    .getItem();

  if (!studyProgram) {
    throw new DataNotFoundError('The relevant StudyProgram cannot be found or is inaccessible.');
  }


  if (studyProgram.currentYear == null) {
    throw new DataError('E_ACADEMIC_YEAR', 'Cannot create course class. Current academic year of a study program cannot be empty at this context')
  }

  if (studyProgram.currentPeriod == null) {
    throw new DataError('E_ACADEMIC_PERIOD', 'Cannot create course class. Current academic period of a study program cannot be empty at this context')
  }

  // create course class
  const newCourseClass = {
    title: course.name,
    course: course.id,
    year: studyProgram.currentYear.id,
    period: studyProgram.currentPeriod.id,
    department: course.department,
    studyProgram: studyProgram.id,
    status: {
      alternateName: 'open'
    }
  }
  // add also course instructor to courseClassInstructors
  // and let course-class-listener handle it
  if (course.instructor) {
    Object.assign(newCourseClass, {
      instructors: [
        {
          instructor: course.instructor
        }
      ]
    })
  }
  // and save
  return await context.model('CourseClass').save(newCourseClass);

}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return afterSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}
