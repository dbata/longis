import { DataModel } from '@themost/data';
import { DataConflictError } from '../errors';
import { DataNotFoundError } from '@themost/common';


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
	return beforeRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 */
export async function beforeRemoveAsync(event) {
	const context = event.model.context;
	const model = event.model;
	const getReferenceMappings = DataModel.prototype.getReferenceMappings;
	model.getReferenceMappings = async function () {
		const res = await getReferenceMappings.bind(this)();
		// unbind DeleteProgramAction
		const mappings = ['DeleteProgramAction', 'UpdateProgramAction', 'CopyProgramAction'];
		return res.filter((mapping) => {
			return mappings.indexOf(mapping.childModel) < 0;
		});
	};
	/**
	 * get studyProgram
	 * @type {StudyProgram}
	 */
	const studyProgram = await context
		.model('StudyProgram')
		.where('id')
		.equal(event.target.id)
		.expand('specialties', 'groups', 'calculationRules')
		.silent()
		.getTypedItem();
	if (studyProgram == null) {
		throw new DataNotFoundError('The study program cannot be found.');
	}
	const removeRules = [];
	// get program courses complex and simple
	const programCourses = await context
		.model('SpecializationCourse')
		.where('studyProgramCourse/studyProgram')
		.equal(studyProgram.id)
		.select('id')
		.silent()
		.count();
	if (programCourses) {
		throw new DataConflictError(
			context.__(
				'The study program cannot be deleted because it contains at least one specialization course.'
			)
		);
	}
	if (Array.isArray(studyProgram.groups) && studyProgram.groups.length) {
		throw new DataConflictError(
			context.__(
				'The study program cannot be deleted because it contains at least one group.'
			)
		);
	}
	const inscriptionRules = await studyProgram.getProgramInscriptionRules();
	if (inscriptionRules && inscriptionRules.length) {
		// remove inscription rules
		Array.prototype.push.apply(removeRules, inscriptionRules);
	}
	const thesisRules = await studyProgram.getThesisRules();
	if (thesisRules && thesisRules.length) {
		// remove thesis rules
		Array.prototype.push.apply(removeRules, thesisRules);
	}
	const internshipRules = await studyProgram.getInternshipRules();
	if (internshipRules && internshipRules.length) {
		// remove internship rules
		Array.prototype.push.apply(removeRules, internshipRules);
	}
	const graduationRules = await studyProgram.getGraduationRules();
	if (graduationRules && graduationRules.length) {
		// remove graduation rules
		Array.prototype.push.apply(removeRules, graduationRules);
	}
	for (const studyProgramGroup of studyProgram.groups) {
		// get program group registration rules
		const programGroup = context
			.model('ProgramGroup')
			.convert(studyProgramGroup);
		const groupRegistrationRules = await programGroup.getRegistrationRules();
		if (groupRegistrationRules && groupRegistrationRules.length) {
			// and delete them, if any
			Array.prototype.push.apply(removeRules, groupRegistrationRules);
		}
	}
	for (const studyProgramSpecialty of studyProgram.specialties) {
		// get specialty rules
		const specialty = context
			.model('StudyProgramSpecialty')
			.convert(studyProgramSpecialty);
		const specialtyRules = await specialty.getSpecialtyRules();
		if (specialtyRules && specialtyRules.length) {
			// and delete them, if any
			Array.prototype.push.apply(removeRules, specialtyRules);
		}
	}
	if (removeRules && removeRules.length) {
		// assign delete state
		removeRules.map((rule) => {
			Object.defineProperty(rule, '$state', {
				configurable: true,
				enumerable: true,
				writable: true,
				value: 4,
			});
		});
		// and remove all rules
		await context.model('Rule').silent().save(removeRules);
	}
	const semesterRules = await context
		.model('StudyProgramSemesterRule')
		.where('studyProgram')
		.equal(studyProgram.id)
		.getAllItems();
	if (semesterRules && semesterRules.length) {
		// remove semester rules
		await context.model('StudyProgramSemesterRule').remove(semesterRules);
	}
	if (studyProgram.calculationRules && studyProgram.calculationRules.length) {
		// remove calculation rules
		await context
			.model('CalculationRule')
			.remove(studyProgram.calculationRules);
	}

    // get timetable
	const timetable = await context
		.model('TimetableEvent')
		.where('studyPrograms/id')
		.equal(event.target.id)
		.silent()
		.getItem();

	if (timetable) {
		// remove timetable
		await context.model('TimetableEvent').remove(timetable);
	}
	
}
