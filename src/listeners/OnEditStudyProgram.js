import { DataNotFoundError, TraceUtils } from "@themost/common";
import { DataConflictError } from "../errors";

class CourseExamListener {
  /**
   * @param {DataEventArgs} event
   */
  static async beforeSaveAsync(event) {
    const context = event.model.context;

    if (event.state === 2) {
      const target = event.target;
      const previous = event.previous;
      const parent = previous.source || previous.id;

      if (
        (previous &&
          previous.currentYear &&
          previous.currentPeriod &&
          target.currentPeriod.id != previous.currentPeriod.id) ||
        (previous.currentYear &&
          target.currentYear.id != previous.currentYear.id)
      ) {
        const studyProgramCopies = await context
          .model("StudyProgram")
          .where("source")
          .equal(parent)
          .or("id")
          .equal(parent)
          .getItems();

        // check if studyProgram exist at leat one
        if (
          Array.isArray(studyProgramCopies) &&
          studyProgramCopies.length === 0
        ) {
          throw new DataNotFoundError(
            "The specified studyProgram cannot be found."
          );
        }

        // when studyProgramCopies.length is equal to 1 means that there is only one studyProgram
        // meaning  self-studyProgram
        // check for more than one (child, parent)
        if (
          Array.isArray(studyProgramCopies) &&
          studyProgramCopies.length > 1
        ) {
          // check if can change academic year and period
          studyProgramCopies.forEach((item) => {
            if (
              item.id != previous.id &&
              item.currentPeriod.id === target.currentPeriod.id &&
              item.currentYear.id === target.currentYear.id
            ) {
              throw new DataConflictError(
                context.__(
                  "Editing of the studyProgram is not allowed because there is another study program copy for the same academic year and academic period."
                )
              );
            }
          });
        }
      }

      if (
        target &&
        Object.prototype.hasOwnProperty.call(target, "discountCategories") ===
          true &&
        target.discountCategories === null
      ) {
        target.discountCategories = [];
      }
      if (
        target &&
        Object.prototype.hasOwnProperty.call(target, "hasFees") === true &&
        !target.hasFees
      ) {
        target.discountCategories = [];
        target.hasfees = false;
        target.fees = 0;
      }
    }
  }

  /**
   * @param {DataEventArgs} event
   */
  static async afterSaveAsync(event) {
    if (event.state === 2) {
      const context = event.model.context;
      const target = event.target;
      const previous = event.previous;
      // get current specialization courses for the edited studyProgram
      const currentSpecializationCourses = await context
        .model("SpecializationCourse")
        .where("studyProgramCourse/studyProgram")
        .equal(target.id)
        .expand("studyProgramCourse")
        .getItems();
      // get studyProgramSpecialty for the edited studyProgram
      const studyProgramSpecialty = await context
        .model("StudyProgramSpecialty")
        .where("studyProgram")
        .equal(target.id)
        .silent()
        .getItem();
      if (!studyProgramSpecialty) {
        throw new DataNotFoundError(
          "The relevant StudyProgramSpecialty cannot be found or is inaccessible."
        );
      }
      // this field comes in a integer format so we have to convert it to boolean
      if (target.isActive == -1) {
        target.isActive = true;
      }
      // check if at least one of the fields we are interested are changed
      if (
        ((previous.totalECTS && target.totalECTS != previous.totalECTS) ||
          (previous.abbreviation &&
            target.abbreviation != previous.abbreviation) ||
          (previous.name && target.name != previous.name) ||
          (previous.isActive && target.isActive != previous.isActive) ||
          (previous.degreeDescription &&
            target.degreeDescription != previous.degreeDescription) ||
          (previous.gradeScale && target.gradeScale != previous.gradeScale) ||
          (previous.submit && target.submit != previous.submit) ||
          (previous.duration && target.duration != previous.duration)) &&
        Array.isArray(currentSpecializationCourses) &&
        currentSpecializationCourses.length == 1
      ) {
        // create the previous version of the specialization course to be removed
        const oldSpecializationCourse = {
          id: currentSpecializationCourses[0].id,
          course: currentSpecializationCourses[0].studyProgramCourse.course,
          specialtyId: currentSpecializationCourses[0].specialization,
          studyProgramID: target.id,
        };
        await context
          .model("SpecializationCourse")
          .silent()
          .remove(oldSpecializationCourse);
        //create the new specialization course with the appropiate changes
        const newSpecializationCourse = {
          ects: target.totalECTS,
          coefficient: 1,
          units: 1,
          semester: 1,
          hours: target.duration,
          courseType: null,
          studyProgramCourse: {
            course: {
              id: target.abbreviation,
              displayCode: target.abbreviation,
              name: target.name,
              isEnabled: target.isActive,
              notes: target.degreeDescription,
              gradeScale: target.gradeScale,
              submit: target.submit,
              department: target.department,
              hours: target.duration,
              ects: target.totalECTS,
            },
            studyProgram: target.id,
          },
          specialization: studyProgramSpecialty,
          $state: 1,
        };

        // and save
        await context
          .model("SpecializationCourse")
          .save(newSpecializationCourse);
      }
      if (
        previous &&
        previous.currentYear &&
        ((previous.currentPeriod &&
          target.currentPeriod.id != previous.currentPeriod.id) ||
          (previous.currentPeriod &&
            target.currentYear.id != previous.currentYear.id))
      ) {
        // if academic year or academic period have changed update also courseClasses data
        const courses = await context
          .model("StudyProgramCourse")
          .where("studyProgram")
          .equal(target.id)
          .select("course", "id")
          .getItems();

        if (Array.isArray(courses) && courses.length) {
          for (let i = 0; i < courses.length; i++) {
            const courseClass = await context
              .model("CourseClass")
              .where("course")
              .equal(courses[i].course)
              .getItem();

            if (courseClass) {
              const courseClassToUpdate = courseClass;

              // find courseExamClasses if there are any
              const courseExamClass = await context
                .model("CourseExamClass")
                .where("courseClass")
                .equal(courseClass.id)
                .expand("courseExam")
                .getItem();

              const courseExam = courseExamClass.courseExam;

              if (courseExam) {
                // remove courseClass
                try {
                  await context
                    .model("CourseClass")
                    .silent()
                    .remove(courseClass);
                  if (courseExam) {
                    // remove courseExam associated with courseClass
                    try {
                      await context
                        .model("CourseExam")
                        .silent()
                        .remove(courseExam);
                    } catch (e) {
                      TraceUtils.error(e);
                    }
                  }
                } catch (e) {
                  TraceUtils.error(e);
                }
              }

              courseClassToUpdate.year = target.currentYear.id;
              courseClassToUpdate.period = target.currentPeriod.id;
              delete courseClassToUpdate.id;

              // save updated meaning new courseClass
              try {
                await context
                  .model("CourseClass")
                  .silent()
                  .save(courseClassToUpdate);
              } catch (e) {
                TraceUtils.error(e);
              }
            }
          }
        }
      }

      // on edit startDate or/and endDate
      if (
        target.startDate != previous.startDate ||
        target.endDate != previous.endDate
      ) {
        // get timetable
        const timetable = await context
          .model("TimetableEvent")
          .where("studyPrograms/id")
          .equal(target.id)
          .silent()
          .getItem();

        if (timetable) {
          //and update it
          timetable.startDate = target.startDate;
          timetable.endDate = target.endDate;
          const updateTimetable = timetable;

          await context.model("TimetableEvent").save(updateTimetable);
        }
      }
    }
  }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return CourseExamListener.afterSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return CourseExamListener.beforeSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}
