/* eslint-disable no-unused-vars */
import { DataError } from '@themost/common';
import { DataEventArgs, DataObjectState } from '@themost/data';
import { ExpressDataContext } from '@themost/express';

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
  /**
   * @type {ExpressDataContext}
   */
  const context = event.model.context;
  const specializationCourse = event.target;
  if (event.target && event.target.$state === DataObjectState.Insert) {
    let courseType = specializationCourse.courseType;
    if (courseType == null) {
      courseType = await context.model('CourseType').take(1).orderBy('id').silent().getItem();
      Object.assign(specializationCourse, { courseType: courseType.id });
    }
    const studyProgramCourse = specializationCourse.studyProgramCourse;
    if (studyProgramCourse && studyProgramCourse.course) {
      const course = await context.model('Course').where('id').equal(studyProgramCourse.course.id).silent().getItem();
      if (course != null) {
        throw new DataError('E_COURSE_ALREADY_EXISTS', 'The course already exists');
      }

      // assign property $state in studyProgramCourse.course
      Object.assign(studyProgramCourse.course, { '$state': 1 });
      await context.model('Course').insert(studyProgramCourse.course);
    }
  } else if (event.target && event.state === DataObjectState.Update) {
    const course = specializationCourse.studyProgramCourse && specializationCourse.studyProgramCourse.course;
    if (course != null) {
      await context.model('Course').update(course);
    }
  }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}
