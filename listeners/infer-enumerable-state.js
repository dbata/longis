const { DataObjectState } = require('@themost/data');
/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
function beforeSave(event, callback) {
    // find many-to-many associations and set state to 4=deleted for those that are not included in the request
    const { model } = event;
    const attributes = model.attributes.filter((attr) => {
        return attr.many === true && attr.enumerable === true && attr.multiplicity === 'Many' && (typeof attr.editable === 'boolean' ? attr.editable : true);   
    }).filter((attr) => {
        /**
         * @type {import('@themost/data').DataAssociationMapping}
         */
        const mapping = model.inferMapping(attr.name);
        // get only many-to-many associations with merge option enabled
        return mapping.associationType === 'junction';
    }).filter((attr) => {
        return Object.prototype.hasOwnProperty.call(event.target, attr.name) && Array.isArray(event.target[attr.name]);
    });
    if (attributes.length === 0) {
        return callback();
    }
    /**
     * @type {import('@themost/data').DataObject}
     */
    const target = event.model.convert(event.target);
    Promise.sequence(attributes.map((attr) => {
        return async () => {
            const items = event.target[attr.name];
            /**
             * @type {import('@themost/data').DataObjectJunction|import('@themost/data').HasParentJunction}
             */
            const property = target.property(attr.name);
            const associatedModel = property.model;
            const { primaryKey } = associatedModel;
            // get associated items
            const existingItems = await property.select(primaryKey).getItems();
            // find items
            for (const item of items) {
                if (Object.prototype.hasOwnProperty.call(item, primaryKey)) {
                    continue;
                }
                const id = await associatedModel.find(item).select(primaryKey).value();
                if (id) {
                    item[primaryKey] = id;
                } else {
                    throw new Error(`Cannot find associated item. The operation will be canceled.`);
                }
            }
            // try to find if an item is not included in the request
            for (const existingItem of existingItems) {
                const item = items.find((x) => {
                    return x[primaryKey] === existingItem[primaryKey];
                });
                if (item == null) {
                    items.push({
                        $state: DataObjectState.Delete,
                        [primaryKey]: existingItem[primaryKey]
                    });
                }
            }
        }
    })).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
module.exports = {
    beforeSave
}