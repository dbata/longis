import {
  ApplicationService,
  DataError,
  DataNotFoundError,
  TraceUtils,
} from "@themost/common";
import {
  EdmType,
  EdmMapping,
  SchemaLoaderStrategy,
  ModelClassLoaderStrategy,
  DataObject,
} from "@themost/data";
import path from "path";
import { QueryExpression, QueryField, QueryEntity } from "@themost/query";
import { DataConflictError } from "./errors";

function courseExamsReady(context, input) {
  const studyProgram = input;
  try {
    /**
     * @type {import('@themost/data').DataQueryable}
     */
    const q = context.model("CourseExams").asQueryable();
    q.select();
    // pseudo-sql INNER JOIN StudyProgramCourseData ON StudyProgramCourseData.studyProgram = 100
    const StudyProgramCourses = new QueryEntity(
      context.model("StudyProgramCourse").sourceAdapter
    ).as("StudyProgramCourses0");
    q.query
      .join(StudyProgramCourses)
      .with(
        new QueryExpression()
          .where(new QueryField("studyProgram").from(StudyProgramCourses))
          .equal(studyProgram.id)
      );

    // pseudo-sql INNER JOIN CourseData ON StudyProgramCourseData.course = CourseData.id AND CourseData.id = CourseExam.course
    const Courses = new QueryEntity(context.model("Course").sourceAdapter).as(
      "Courses0"
    );
    q.query
      .join(Courses)
      .with(
        new QueryExpression()
          .where(new QueryField("course").from(StudyProgramCourses))
          .equal(new QueryField("id").from(Courses))
          .and(new QueryField("course").from(q.query.$collection))
          .equal(new QueryField("id").from(Courses))
      );

    const CourseExamClasses = new QueryEntity(
      context.model("CourseExamClass").sourceAdapter
    ).as("CourseExamClasses0");
    q.query
      .join(CourseExamClasses)
      .with(
        new QueryExpression()
          .where(new QueryField("courseExam").from(CourseExamClasses))
          .equal(new QueryField("id").from(q.query.$collection))
      );

    const CourseClasses = new QueryEntity(
      context.model("CourseClass").sourceAdapter
    ).as("CourseClasses0");
    q.query
      .join(CourseClasses)
      .with(
        new QueryExpression()
          .where(new QueryField("courseClass").from(CourseExamClasses))
          .equal(new QueryField("id").from(CourseClasses))
      );

    q.query
      .where("CourseClasses0.year")
      .equal(studyProgram.currentYear.id)
      .and("CourseClasses0.period")
      .equal(studyProgram.currentPeriod.id)
      .prepare();
    return q;
  } catch (err) {
    TraceUtils.error(err);
    throw err;
  }
}

class StudyProgram extends DataObject {
  /**
   * @returns {DataQueryable}
   */
  @EdmMapping.func("events", EdmType.CollectionOf("Event"))
  async getEvents() {
    try {
      /**
       * @type {import('@themost/data').DataQueryable}
       */
      const q = this.context.model("Event").asQueryable();
      q.select();
      const Events = new QueryEntity(q.query.$collection);
      // ensure that the association between TimeTableEvent and StudyProgram has been already created
      // try to upgrade TimetableEvent
      await this.context.model("TimetableEvent").migrateAsync();
      // try to upgrade TimeTableStudyPrograms which is the model that holds the association between
      // TimetableEvent and StudyProgram
      /**
       * @type {import('@themost/data').DataModel}
       */
      const TimeTableStudyPrograms = this.context
        .model("TimetableEvent")
        .convert({ id: 0 })
        .property("studyPrograms")
        .getBaseModel();
      await TimeTableStudyPrograms.migrateAsync();
      const StudyProgramTimeTableEvents = new QueryEntity(
        TimeTableStudyPrograms.sourceAdapter
      ).as("StudyProgramTimeTableEvents");
      // pseudo-sql: INNER JOIN TimeTableStudyPrograms AS StudyProgramTimeTableEvents
      // ON StudyProgramTimeTableEvents.parentId = <id>
      const parent = TimeTableStudyPrograms.fields.find(
        (x) => x.type === "StudyProgram"
      );
      q.query
        .join(StudyProgramTimeTableEvents)
        .with(
          new QueryExpression()
            .where(
              new QueryField(parent.name).from(StudyProgramTimeTableEvents)
            )
            .equal(this.getId())
        );
      const child = TimeTableStudyPrograms.fields.find(
        (x) => x.type === "TimetableEvent"
      );
      const TimeTableEvents = new QueryEntity(
        this.context.model("TimetableEvent").sourceAdapter
      ).as("TimeTableEvents0");
      // pseudo-sql: INNER JOIN TimeTableEvents AS TimeTableEvents0
      // ON StudyProgramTimeTableEvents.valueId = TimeTableEvents.id
      // AND Events.superEvent = TimeTableEvents0.id
      q.query
        .join(TimeTableEvents)
        .with(
          new QueryExpression()
            .where(new QueryField(child.name).from(StudyProgramTimeTableEvents))
            .equal(new QueryField("id").from(TimeTableEvents))
            .and(new QueryField("superEvent").from(Events))
            .equal(new QueryField("id").from(TimeTableEvents))
        );
      return q;
    } catch (err) {
      TraceUtils.error(err);
      throw err;
    }
  }

  /**
   * @returns {DataQueryable}
   */
  @EdmMapping.func("classes", EdmType.CollectionOf("CourseClass"))
  async getCourseClasses() {
    const studyProgramID = this.getId();
    const studyProgram = await this.context
      .model("StudyProgram")
      .where("id")
      .equal(studyProgramID)
      .getItem();

    if (!studyProgram) {
      throw new DataNotFoundError(
        "The specified studyProgram cannot be found."
      );
    }

    try {
      /**
       * @type {import('@themost/data').DataQueryable}
       */
      const q = this.context.model("CourseClass").asQueryable();
      q.select();
      // pseudo-sql INNER JOIN StudyProgramCourseData ON StudyProgramCourseData.studyProgram = 100
      const StudyProgramCourses = new QueryEntity(
        this.context.model("StudyProgramCourse").sourceAdapter
      ).as("StudyProgramCourses0");
      q.query
        .join(StudyProgramCourses)
        .with(
          new QueryExpression()
            .where(new QueryField("studyProgram").from(StudyProgramCourses))
            .equal(this.getId())
        );
      // pseudo-sql INNER JOIN CourseData ON StudyProgramCourseData.course = CourseData.id AND CourseData.id = CourseClass.course
      const Courses = new QueryEntity(
        this.context.model("Course").sourceAdapter
      ).as("Courses0");
      q.query
        .join(Courses)
        .with(
          new QueryExpression()
            .where(new QueryField("course").from(StudyProgramCourses))
            .equal(new QueryField("id").from(Courses))
            .and(new QueryField("course").from(q.query.$collection))
            .equal(new QueryField("id").from(Courses))
        );
      q.query
        .where("year")
        .equal(studyProgram.currentYear.id)
        .and("period")
        .equal(studyProgram.currentPeriod.id)
        .prepare();
      return q;
    } catch (err) {
      TraceUtils.error(err);
      throw err;
    }
  }

  @EdmMapping.func("exams", EdmType.CollectionOf("CourseExams"))
  async getCourseExams() {
    const studyProgramID = this.getId();
    const studyProgram = await this.context
      .model("StudyProgram")
      .where("id")
      .equal(studyProgramID)
      .getItem();

    if (!studyProgram) {
      throw new DataNotFoundError(
        "The specified studyProgram cannot be found."
      );
    }

    return courseExamsReady(this.context, studyProgram);
  }

  /**
   * @returns {DataQueryable}
   */
  @EdmMapping.func(
    "examDocumentActions",
    EdmType.CollectionOf("ExamDocumentUploadActions")
  )
  async getExamDocumentActions() {
    const studyProgramID = this.getId();
    const studyProgram = await this.context
      .model("StudyProgram")
      .where("id")
      .equal(studyProgramID)
      .getItem();

    if (!studyProgram) {
      throw new DataNotFoundError(
        "The specified studyProgram cannot be found."
      );
    }

    try {
      /**
       * @type {import('@themost/data').DataQueryable}
       */
      const q = this.context.model("ExamDocumentUploadActions").asQueryable();
      q.select();

      const studyProgramCourseAdapter =
        this.context.model("StudyProgramCourse").sourceAdapter;
      const StudyProgramCourses = new QueryEntity(studyProgramCourseAdapter).as(
        "StudyProgramCourses0"
      );
      // INNER JOIN StudyProgram ON StudyProgramData.id = StudyProgramCourse.studyProgram
      q.query
        .join(StudyProgramCourses)
        .with(
          new QueryExpression()
            .where(new QueryField("studyProgram").from(StudyProgramCourses))
            .equal(studyProgram.id)
        );

      const coursesAdapter = this.context.model("Course").sourceAdapter;
      const Courses = new QueryEntity(coursesAdapter).as("Courses0");
      // INNER JOIN StudyProgramCourse ON StudyProgramCourse.course = Course.id
      q.query
        .join(Courses)
        .with(
          new QueryExpression()
            .where(new QueryField("course").from(StudyProgramCourses))
            .equal(new QueryField("id").from(Courses))
        );

      const courseExamsAdapter = this.context.model("CourseExam").sourceAdapter;
      const CourseExams = new QueryEntity(courseExamsAdapter).as(
        "CourseExams0"
      );
      q.query
        .join(CourseExams)
        .with(
          new QueryExpression()
            .where(new QueryField("course").from(CourseExams))
            .equal(new QueryField("id").from(Courses))
            .and(new QueryField("object").from(q.query.$collection))
            .equal(new QueryField("id").from(CourseExams))
        );

      const CourseExamClassAdapter =
        this.context.model("CourseExamClass").sourceAdapter;
      const CourseExamClasses = new QueryEntity(CourseExamClassAdapter).as(
        "CourseExamClasses0"
      );
      q.query
        .join(CourseExamClasses)
        .with(
          new QueryExpression()
            .where(new QueryField("courseExam").from(CourseExamClasses))
            .equal(new QueryField("id").from(CourseExams))
        );

      const CourseClassAdapter =
        this.context.model("CourseClass").sourceAdapter;
      const CourseClasses = new QueryEntity(CourseClassAdapter).as(
        "CourseClasses0"
      );
      q.query
        .join(CourseClasses)
        .with(
          new QueryExpression()
            .where(new QueryField("courseClass").from(CourseExamClasses))
            .equal(new QueryField("id").from(CourseClasses))
        );

      q.query
        .where("CourseClasses0.year")
        .equal(studyProgram.currentYear.id)
        .and("CourseClasses0.period")
        .equal(studyProgram.currentPeriod.id)
        .prepare();
      return q;
    } catch (err) {
      TraceUtils.error(err);
      throw err;
    }
  }

  /**
   * @returns {StudyProgram}
   */
  @EdmMapping.param("data", "Object", false, true)
  @EdmMapping.action("copy", "StudyProgram")
  async copy(data) {
    // check all students
    const action = await this.context.model("CopyProgramAction").silent().save({
      studyProgram: this.getId(),
    });
    await this.copyProgram(this.context, this.getId(), action, data);
    return action;
  }

  copyProgram(appContext, program, action, data) {
    const app = appContext.getApplication();
    const context = app.createContext();
    context.user = appContext.user;
    (async function () {
      /**
       * get studyProgram
       * @type {StudyProgram}
       */
      const studyProgram = await context
        .model("StudyProgram")
        .asQueryable()
        .expand(
          "specialties",
          "groups",
          "discountCategories",
          "calculationRules",
          {
            name: "info",
            options: {
              $expand: "locales",
            },
          }
        )
        .where("id")
        .equal(program)
        .silent()
        .getTypedItem();

      // check if the user has set the same academic year and academic period for the new studyProgram
      // as the original studyProgram (eg studyProgram to be copied)
      // or if there is a studyProgram child (for the same year and period)
      // in this case study studyProgram cannot be created
      const parent = studyProgram.source || studyProgram.id;
      const studyProgramCopies = await context
        .model("StudyPrograms")
        .where("source")
        .equal(parent)
        .or("id")
        .equal(parent)
        .getItems();

      if (
        Array.isArray(studyProgramCopies) &&
        studyProgramCopies.length === 0
      ) {
        throw new DataNotFoundError(
          "The specified studyProgram cannot be found."
        );
      }

      if (Array.isArray(studyProgramCopies) && studyProgramCopies.length > 1) {
        // check if academic year and period of input are allowed
        studyProgramCopies.forEach((item) => {
          if (
            item.id != data.id &&
            item.currentPeriod.id === data.currentPeriod.id &&
            item.currentYear.id === data.currentYear.id
          ) {
            throw new DataConflictError(
              context.__(
                "Copy of the studyProgram is not allowed because there is another study program copy for the same academic year and academic period."
              )
            );
          }
        });
      }

      if (
        studyProgram.currentYear.id === data.currentYear.id &&
        studyProgram.currentPeriod.id === data.currentPeriod.id
      ) {
        throw new DataError(
          "E_INVALID_DATA",
          "The study program cannot be copied for the same academic year and academic period.",
          null,
          "TimetableEvent"
        );
      }

      // get program group rules
      if (studyProgram.groups && studyProgram.groups.length) {
        for (let i = 0; i < studyProgram.groups.length; i++) {
          /**
           * @type {ProgramGroup}
           */
          const programGroup = context
            .model("ProgramGroup")
            .convert(studyProgram.groups[i]);
          studyProgram.groups[i].rules =
            await programGroup.getRegistrationRules();
        }
      }
      // get specialty rules
      if (studyProgram.specialties && studyProgram.specialties.length) {
        for (let i = 0; i < studyProgram.specialties.length; i++) {
          /**
           * @type {StudyProgramSpecialty}
           */
          const specialty = context
            .model("StudyProgramSpecialty")
            .convert(studyProgram.specialties[i]);
          studyProgram.specialties[i].rules =
            await specialty.getSpecialtyRules();
        }
      }
      // remove attributes
      studyProgram.name =
        (data && data.description) || studyProgram.name + "-Copy";
      studyProgram.isActive = 1;
      // get program courses complex and simple
      const programCourses = await context
        .model("SpecializationCourse")
        .where("studyProgramCourse/studyProgram")
        .equal(studyProgram.id)
        .equal(studyProgram.id)
        .and("studyProgramCourse/course/courseStructureType")
        .in([1, 4])
        .expand({
          name: "studyProgramCourse",
          options: {
            $expand: "course",
          },
        })
        .orderBy("specializationIndex")
        .getAllItems();
      if (programCourses.length) {
        for (let i = 0; i < programCourses.length; i++) {
          /**
           * @type {StudyProgramCourse}
           */
          const programCourse = context
            .model("SpecializationCourse")
            .convert(programCourses[i]);

          programCourses[i].rules =
            await programCourse.getProgramCourseRegistrationRules();
        }
      }
      // set attributes of studyProgram-copy
      studyProgram.programCourses = programCourses;
      studyProgram.inscriptionRules =
        await studyProgram.getProgramInscriptionRules();
      studyProgram.thesisRules = await studyProgram.getThesisRules();
      studyProgram.internshipRules = await studyProgram.getInternshipRules();
      studyProgram.graduationRules = await studyProgram.getGraduationRules();
      studyProgram.currentPeriod = data.currentPeriod;
      studyProgram.currentYear = data.currentYear;
      studyProgram.startDate = data.startDate;
      studyProgram.endDate = data.endDate;
      // When the user creates a studyProgram copy from a copy,
      // the original studyProgram will be considered as the parent studyProgram
      studyProgram.source = studyProgram.source || program;

      delete studyProgram.id;
      // return new studyProgram
      return await context.model("StudyProgram").save(studyProgram);
    })()
      .then((result) => {
        context.finalize(() => {
          // build result
          // after finishing copying program, update actionStatus
          action.actionStatus = { alternateName: "CompletedActionStatus" };
          action.endTime = new Date();
          action.result = result;
          /* eslint-disable */ // not display error for no-unused-vars rule
          return context
            .model("CopyProgramAction")
            .silent()
            .save(action)
            .then((action) => {})
            .catch((err) => {
              TraceUtils.error(err);
            });
        });
      })
      .catch((err) => {
        context.finalize(() => {
          action.actionStatus = { alternateName: "FailedActionStatus" };
          action.endTime = new Date();
          action.description = err.message;
          return context
            .model("CopyProgramAction")
            .silent()
            .save(action)
            .then((action) => {
              /* eslint-enable */
            })
            .catch((err) => {
              TraceUtils.error(
                `An error occurred while copying study program with id ${program}`
              );
              TraceUtils.error(err);
            });
        });
      });
  }
}

class StudyProgramReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication()
      .getConfiguration()
      .getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition("StudyProgram");
    if (model) {
      model.eventListeners = model.eventListeners || [];
      const insertIndex = model.eventListeners.findIndex(
        (item) => item.type === "@themost/data/previous-state-listener"
      );
      // add extra listeners after the '@themost/data/previous-state-listener' listener
      const insertListener = {
        type: path.resolve(
          __dirname,
          "listeners/after-save-create-timetable-listener"
        ),
      };
      model.eventListeners.splice(insertIndex + 1, 0, insertListener);
      model.eventListeners.splice(insertIndex + 2, 0, {
        type: path.resolve(__dirname, "listeners/OnEditStudyProgram"),
      });
      model.eventListeners.push({
        type: path.resolve(
          __dirname,
          "listeners/auto-create-specialization-course-listener"
        ),
      });
      model.eventListeners.push({
        type: path.resolve(__dirname, "listeners/OnSelectStudyProgram"),
      });

      const replaceListenerIndex = model.eventListeners.findIndex(
        (item) =>
          item.type === "./listeners/before-remove-study-program-listener"
      );
      model.eventListeners.splice(replaceListenerIndex, 1, {
        type: path.resolve(
          __dirname,
          "listeners/before-remove-study-program-listener"
        ),
      });
      schemaLoader.setModelDefinition(model);

      // get model class
      const loader = this.getApplication()
        .getConfiguration()
        .getStrategy(ModelClassLoaderStrategy);
      const StudyProgramClass = loader.resolve(model);
      StudyProgramClass.prototype.getCourseClasses =
        StudyProgram.prototype.getCourseClasses;
      StudyProgramClass.prototype.getEvents = StudyProgram.prototype.getEvents;
      StudyProgramClass.prototype.getCourseExams =
        StudyProgram.prototype.getCourseExams;
      StudyProgramClass.prototype.getExamDocumentActions =
        StudyProgram.prototype.getExamDocumentActions;
      // adds copy and copyProgram methods for longis
      StudyProgramClass.prototype.copy = StudyProgram.prototype.copy;
      StudyProgramClass.prototype.copyProgram =
        StudyProgram.prototype.copyProgram;
    }
  }
}

export { StudyProgramReplacer };
