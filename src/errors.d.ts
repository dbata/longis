import { DataError, HttpError } from "@themost/common";

export declare class GenericDataConflictError extends DataError {
    constructor(code: string, message?: string, innerMessage?: string, model?: string);
}

export declare class DataConflictError extends GenericDataConflictError {
    constructor(message?: string, innerMessage?: string, model?: string);
}