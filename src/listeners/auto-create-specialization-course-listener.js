/* eslint-disable no-unused-vars */
import { DataNotFoundError } from "@themost/common";
import { DataEventArgs } from "@themost/data";
import { ExpressDataContext } from "@themost/express";

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
  /**
   * @type {ExpressDataContext}
   */
  const context = event.model.context;
  const studyProgram = event.target;
  // get studyProgramSpecialty
  const studyProgramSpecialty = await context
    .model("StudyProgramSpecialty")
    .where("studyProgram")
    .equal(studyProgram.id)
    .silent()
    .getItem();
  if (!studyProgramSpecialty) {
    throw new DataNotFoundError(
      "The relevant StudyProgramSpecialty cannot be found or is inaccessible."
    );
  }
  if (event.state === 1) {
    // if it is not a copy action
    if (studyProgram.source == null) {
      // create specialization course
      const newSpecializationCourse = {
        ects: studyProgram.totalECTS,
        coefficient: 1,
        units: 1,
        semester: 1,
        hours: studyProgram.duration,
        courseType: null,
        studyProgramCourse: {
          course: {
            id: studyProgram.abbreviation,
            displayCode: studyProgram.abbreviation,
            name: studyProgram.name,
            isEnabled: studyProgram.isActive,
            notes: studyProgram.degreeDescription,
            gradeScale: studyProgram.gradeScale,
            submit: studyProgram.submit,
            department: studyProgram.department,
            hours: studyProgram.duration,
            ects: studyProgram.totalECTS,
          },
          studyProgram: studyProgram.id,
        },
        specialization: studyProgramSpecialty,
        $state: 1,
      };
      // and save
      return await context
        .model("SpecializationCourse")
        .save(newSpecializationCourse);
    }
  }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return afterSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}
