// eslint-disable-next-line no-unused-vars
import { DataEventArgs } from '@themost/data';
import { Args } from '@themost/common';
/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	if (event.state !== 1) {
		return;
	}
	const context = event.model.context;
	// get new studyProgram from current transaction
	const studyProgram = event.target;
	
	const eventTypes = await context.model('EventType').getItems();

	const academicYear = await context.model('StudyProgram').where('id').equal(studyProgram.id).select('currentYear').value() || studyProgram.currentYear;
	Args.check(academicYear != null, new Error('The current academic year of a study program cannot be empty at this context'));
	
	const academicPeriod = await context.model('StudyProgram').where('id').equal(studyProgram.id).select('currentPeriod').value() || studyProgram.currentPeriod;
	Args.check(academicPeriod != null, new Error('The current academic period of a study program cannot be empty at this context'));

	// save new timetable
	const newTimetable = {
		startDate: studyProgram.startDate || null,
		endDate: studyProgram.endDate || null,
		studyPrograms: [
			studyProgram.id
		], 
		academicYear: academicYear,
		academicPeriods: [
			academicPeriod
		],
		name: studyProgram.name,
		organizer: studyProgram.department,
		availablePlaces: [],
		availableEventTypes: eventTypes
	};

	await context.model('TimetableEvent').save(newTimetable);

}

export function afterSave(event, callback) {
	afterSaveAsync(event).then(() => {
		return callback();
	}).catch((err) => {
		return callback(err);
	});
}