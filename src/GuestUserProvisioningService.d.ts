import { ApplicationBase, ApplicationService } from '@themost/common';
import { DataContext } from '@themost/data';

export declare class GuestUserProvisioningService extends ApplicationService {
    constructor(app: ApplicationBase);
    getUser(context: DataContext, info: {
        scope: string,
        username: string,
        preferred_username: string,
        email: string,
        name: string,
        family_name: string,
        given_name: string
    }): Promise<any>;
}