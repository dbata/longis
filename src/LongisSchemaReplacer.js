import { FileSchemaLoaderStrategy, SchemaLoaderStrategy, ODataModelBuilder } from '@themost/data';
import { ApplicationService } from '@themost/common';
import path from 'path';
/**
 * A schema loader for loading data models associated with document numbering services
 */
export class SchemaReplacerLoader extends FileSchemaLoaderStrategy {

    /**
     * @param {ConfigurationBase} config
     */
    constructor(config) {
        super(config);
        this.setModelPath(path.resolve(__dirname, 'config/replace/models'));
    }

    getModelDefinition(name) {
        return super.getModelDefinition.bind(this)(name);
    }

}

export class LongisSchemaReplacer extends ApplicationService {

    constructor(app) {
        super(app);
        // get application schema loader
        const schemaLoader = app.getConfiguration().getStrategy(SchemaLoaderStrategy);
        // create replacer
        const replacer = new SchemaReplacerLoader(app.getConfiguration());
        // get models
        const models = replacer.readSync();
        // loop throught replacer models
        models.forEach((model) => {
            // get local model definition
            const modelDefinition = replacer.getModelDefinition(model);
            // get original model definition
            const originalDefinition = schemaLoader.getModelDefinition(model);
            // if original model version is higher, do nothing
            if (modelDefinition.version < originalDefinition.version) {
                return;
            }
            // and replace root model definition
            schemaLoader.setModelDefinition(modelDefinition);
        });
        const builder = app.getService(ODataModelBuilder);
        if (builder != null) {
            // cleanup builder and wait for next call
            builder.clean(true);
            builder.initializeSync();
        }
    }

}
