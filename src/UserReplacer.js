import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';

class UserReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('User');
        const findAttribute = model.fields.find((field) => {
            return field.name === 'studyPrograms'
        });
        if (findAttribute == null) {
            model.fields.push({
                'name': 'studyPrograms',
                'description': 'A collection of study programs where user has access to',
                'type': 'StudyProgram',
                'mapping': {
                  'associationAdapter': 'UserStudyPrograms',
                  'parentModel': 'StudyProgram',
                  'associationObjectField': 'studyProgram',
                  'associationValueField': 'user',
                  'parentField': 'id',
                  'childModel': 'User',
                  'childField': 'id',
                  'associationType': 'junction',
                  'select': [
                    'id',
                    'name'
                  ]
                }
              });
            schemaLoader.setModelDefinition(model);
        }
    }

}

export {
    UserReplacer
}
