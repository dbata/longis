import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';    

class CourseExamReplacer extends ApplicationService {
    constructor(app) {
        super(app);
        this.model = 'CourseExam'
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition(this.model);
        if (model) {
            model.eventListeners = model.eventListeners || [];
            // add extra listener
            model.eventListeners.push({
              type: path.resolve(__dirname, 'listeners/OnSelectCourseExam')
            });
            schemaLoader.setModelDefinition(model);
          }
    }

}

export {
    CourseExamReplacer
}
