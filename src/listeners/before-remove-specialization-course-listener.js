// import { DataModel } from '@themost/data';
import { DataNotFoundError, TraceUtils } from '@themost/common';
import { DataConflictError } from '../errors';


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
	return beforeRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 */
export async function beforeRemoveAsync(event) {
	const context = event.model.context;

	/**
	 * get specializationCourse
	 * @type {SpecializationCourse}
	 */
	const specializationCourse = await context
		.model('SpecializationCourse')
		.where('id')
		.equal(event.target.id)
		.expand({
			'name': 'studyProgramCourse',
			'options':
			{
				'$expand': 'studyProgram'
			}
		})
		.silent()
		.getItem();

	/**
	 * get studyProgramCourses
	 * @type {StudyProgramCourses[]}
	 */
	const studyProgramCourses = await context
		.model('StudyProgramCourse')
		.where('course')
		.equal(specializationCourse.studyProgramCourse.course)
		.and('studyProgram/id')
		.equal(event.target.studyProgramID)
		.silent()
		.getItems();

	if (!Array.isArray(studyProgramCourses) || studyProgramCourses.length === 0) {
		throw new DataNotFoundError('The studyProgramCourse cannot be found.');
	}

	const studyPrograms = studyProgramCourses.map(item => item.studyProgram)

	// check if the course can be deleted 
	//(it does not belong to another study program, for the same academic year and academic period)
	studyPrograms.forEach(item => {
		if (item.id != specializationCourse.studyProgramCourse.studyProgram.id
			&& item.currentPeriod === specializationCourse.studyProgramCourse.studyProgram.currentPeriod
			&& item.currentYear === specializationCourse.studyProgramCourse.studyProgram.currentYear) {
			throw new DataConflictError(context.__('The specialization course cannot be deleted because it belongs to another study program, for the same academic year and academic period.'));
		}
	});
}


export function afterRemove(event, callback) {
	afterRemoveAsync(event).then(() => {
		return callback();
	}).catch((err) => {
		return callback(err);
	});
}


async function afterRemoveAsync(event) {
	const context = event.model.context;

	const studyProgram = await context.model('StudyProgram').where('id').equal(event.target.studyProgramID).getItem();
	if (!studyProgram) {
		throw new DataNotFoundError('The specified studyProgram cannot be found.')
	}

	// check if there is a courseExam associated and remove it
	const courseExams = await context
		.model('CourseExam')
		.where('course')
		.equal(event.target.course)
		.and('year')
		.equal(studyProgram.currentYear)
		.and('examPeriod/academicPeriods')
		.equal(studyProgram.currentPeriod)
		.silent()
		.getItems();

	if (Array.isArray(courseExams) && courseExams.length === 1) {
		try {
			await context.model('CourseExam').remove({ id: courseExams[0].id });
		} catch (e) {
			TraceUtils.error(e);
		}
	}

	// check if courseClass should be removed too
	const courseClasses = await context
		.model('CourseClass')
		.where('course')
		.equal(event.target.course)
		.and('studyProgram/id')
		.equal(studyProgram.id)
		.silent()
		.getItems();

	// should be only one, therefore check if can be removed
	if (Array.isArray(courseClasses) && courseClasses.length === 1) {
		const courseClassToRemove = courseClasses[0];
		if (courseClassToRemove.year.id === studyProgram.currentYear.id && courseClassToRemove.period === studyProgram.currentPeriod.id) {
			try {
				await context.model('CourseClass').silent().remove({ id: courseClassToRemove.id });
			} catch (e) {
				TraceUtils.error(e);
			}
		}
	}

	// check if course should be deleted too
	const studyProgramCourses = await context
		.model('StudyProgramCourse')
		.where('course')
		.equal(event.target.course)
		.silent()
		.getItems();

	// should be none, therefore check if course can be removed
	if (Array.isArray(studyProgramCourses) && studyProgramCourses.length === 0) {
		try {
			await context.model('Course').remove({ id: event.target.course });
		} catch (e) {
			TraceUtils.error(e);
		}
	}

}

